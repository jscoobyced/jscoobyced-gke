import { createDefaultMock } from '../testUtil';
import { logger } from '../utils/logger';
import { requestLogger } from './logger';

jest.mock('../utils/logger');

describe('console logger middleware', () => {
  it('proceeds to next middleware', () => {
    const { mockRequest, mockResponse } = createDefaultMock();
    const next = jest.fn();
    requestLogger(mockRequest, mockResponse, next);
    expect(next).toHaveBeenCalledTimes(1);
  });

  it('can log the body of request', () => {
    const { mockRequest, mockResponse } = createDefaultMock({ id: 100 });
    const next = jest.fn();
    requestLogger(mockRequest, mockResponse, next);
    expect(logger.info).toHaveBeenLastCalledWith('undefined undefined\n\tbody: {"id":100}');
  });
});
