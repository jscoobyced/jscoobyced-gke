import { Route } from '../models/route';
import { routes as indexRoute } from '.';

export const routes: Route[] = [...indexRoute];
