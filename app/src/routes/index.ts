import { Route } from '../models/route';
import { index } from '../controllers/index/index';
import { requestLogger } from '../middleware/logger';

export const routes: Route[] = [
  {
    method: 'get',
    path: '/',
    middleware: [requestLogger],
    handler: index,
  },
];
