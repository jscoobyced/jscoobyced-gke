import { Handler } from '../../models/route';
import os from 'os';

export const index: Handler = (req, res) => {
  res.send(`DOTFLAT API from ${os.hostname}`).status(200);
  return Promise.resolve();
};
