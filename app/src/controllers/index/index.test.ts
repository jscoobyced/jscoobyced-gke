import { index } from '.';
import os from 'os';
import { createDefaultMock } from '../../testUtil';

describe('index', () => {
  it('returns hello world', () => {
    const { mockRequest, mockResponse } = createDefaultMock();
    index(mockRequest, mockResponse);
    expect(mockResponse.send).toHaveBeenCalledWith(`DOTFLAT API from ${os.hostname}`);
  });
});
