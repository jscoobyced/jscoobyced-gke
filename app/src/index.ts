import express from 'express';
import cors from 'cors';
import helmet from 'helmet';
import * as dotenv from 'dotenv';
import { routes } from './routes/routes';

dotenv.config();

const app = express();
const PORT = process.env.BACK_END_API_PORT || 3000;

const corsOptions = {
  origin: '*',
  optionsSuccessStatus: 200,
  methods: ['GET', 'POST', 'PUT'],
};

app.use(cors(corsOptions));
app.use(helmet());
app.disable('x-powered-by');

const extended = process.env.NODE_ENV === 'test';
app.use(express.json());
app.use(express.urlencoded({ extended }));

routes.forEach((route) => {
  const { version, method, path, middleware, handler } = route;
  let fullPath = path;
  if (!!version) {
    fullPath = `${version}${path}`;
  }
  app[method](fullPath, ...middleware, handler);
});

if (process.env.NODE_ENV !== 'test') {
  app.listen(PORT, () => {
    console.log(`Server started at http://localhost:${PORT}`);
  });
}

export default app;
