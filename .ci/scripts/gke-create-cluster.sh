#!/bin/bash

TMPPATH=$(mktemp --directory)
export KUBECONFIG=${TMPPATH}/.config
source ${CI_PROJECT_DIR}/.ci/scripts/gke-login.sh
gke-login

GKE_ZONE=${GKE_CLUSTER_ZONE}

if [ "${GKE_AUTOPILOT}" = "Y" ]
then
  GKE_ZONE=${GKE_CLUSTER_REGION}
  gcloud container clusters create-auto ${GKE_CLUSTER_NAME} \
      --zone=${GKE_ZONE} \
      --project=${GKE_PROJECT}
else
  gcloud container clusters create ${GKE_CLUSTER_NAME} \
      --zone=${GKE_ZONE} \
      --machine-type=${GKE_MACHINE_TYPE} \
      --no-enable-ip-alias \
      --disk-size=${GKE_DISK_SIZE} \
      --project=${GKE_PROJECT}
fi

if [ $? -eq 0 ]
then
  gcloud container clusters get-credentials --zone=${GKE_ZONE} ${GKE_CLUSTER_NAME}
fi

if [ "$1" = "--verbose" ]
then
  gcloud container clusters --zone=${GKE_ZONE} describe ${GKE_CLUSTER_NAME}
  gcloud container clusters list
fi

rm -Rf ${TMPPATH}