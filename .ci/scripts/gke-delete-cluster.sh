#!/bin/bash

TMPPATH=$(mktemp --directory)
export KUBECONFIG=${TMPPATH}/.config
source ${CI_PROJECT_DIR}/.ci/scripts/gke-login.sh
gke-login

GKE_ZONE=${GKE_CLUSTER_ZONE}

if [ "${GKE_AUTOPILOT}" = "Y" ]
then
  GKE_ZONE=${GKE_CLUSTER_REGION}
fi

gcloud container clusters delete ${GKE_CLUSTER_NAME} --zone ${GKE_ZONE}

rm -Rf ${TMPPATH}