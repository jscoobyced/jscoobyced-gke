#!/bin/bash

TMPPATH=$(mktemp --directory)
export KUBECONFIG=${TMPPATH}/.config

source ${CI_PROJECT_DIR}/.ci/scripts/gke-login.sh

gke-login

# Create custom namespace for easy filtering
kubectl create namespace ${GKE_NAMESPACE}

# Deploy the load balancer service
kubectl apply -f ${CI_PROJECT_DIR}/.ci/k8s/service.yaml

# Initial deployment of the pods with ":latest" image tag
kubectl apply -f ${CI_PROJECT_DIR}/.ci/k8s/application.yaml

rm -Rf ${TMPPATH}