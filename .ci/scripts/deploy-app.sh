#!/bin/bash

TMPPATH=$(mktemp --directory)
export KUBECONFIG=${TMPPATH}/.config

source ${CI_PROJECT_DIR}/.ci/scripts/gke-login.sh

gke-login

APP_TAG=$1

kubectl patch deployments ${GKE_DEPLOYMENT} --namespace=${GKE_NAMESPACE} \
    -p"{\"spec\":{\"template\":{\"spec\":{\"containers\":[{\"name\":\"${GKE_CONTAINER_NAME}\",\"image\":\"${REGISTRY_URL}/${APP_NAME}:${APP_TAG}\"}]}}}}"


rm -Rf ${TMPPATH}