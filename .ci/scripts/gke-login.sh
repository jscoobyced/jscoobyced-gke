#!/bin/bash

gke-login() {
    echo "${GKE_SERVICE_ACCOUNT}" | base64 -d > ${TMPPATH}/encoded_serviceaccount.json
    echo "Activating account"
    gcloud auth activate-service-account --key-file ${TMPPATH}/encoded_serviceaccount.json
    echo "Setting project ${GKE_PROJECT}"
    gcloud config set project ${GKE_PROJECT}
    echo "Updating credentials for zone ${GKE_CLUSTER_ZONE} and cluster ${GKE_CLUSTER_NAME}"
    gcloud container clusters get-credentials --zone=${GKE_CLUSTER_ZONE} ${GKE_CLUSTER_NAME}
}
