#!/bin/bash

TMPPATH=$(mktemp --directory)
export KUBECONFIG=${TMPPATH}/.config

source ${CI_PROJECT_DIR}/.ci/scripts/gke-login.sh

gke-login

kubectl delete -f ${CI_PROJECT_DIR}/.ci/k8s/service.yaml
kubectl delete -f ${CI_PROJECT_DIR}/.ci/k8s/application.yaml
kubectl delete namespace ${GKE_NAMESPACE}

rm -Rf ${TMPPATH}