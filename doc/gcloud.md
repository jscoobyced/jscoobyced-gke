Install gcloud by following https://cloud.google.com/sdk/docs/install:
- Download the tar.gz
- Extract
- In a terminal run <gcloud-folder>/install.sh
- Close the terminal and open again
- Run the command
```
gcloud components install kubectl
```

Change to this project root directory:
```
cp .env.example .env
# Update the GKE_SERVICE_ACCOUNT with the correct key
source .env
TMPPATH=$(mktemp --directory)
export KUBECONFIG=${TMPPATH}/.config
source .ci/scripts/gke-login.sh
gke-login
# The output should show no warning or error
# The output should end with a message like "kubeconfig entry generated for <GKE_CLUSTER_NAME>."
# If you close the terminal you will need to start again

# Test everything is working (get the cluster info):
gcloud container clusters list --zone ${GKE_CLUSTER_ZONE}

# Optional: cleanup the temporary folder before closing
rm -Rf ${TMPPATH}

```