# Node API in GKE

This API is deployed on GKE.

## Back-End API

The API is a node application, using the express server. The documentation to add a new endpoint is available [here](https://github.com/jscoobyced/full-stack-app/tree/main/doc/be).

## Run locally in production mode

Simply run the `docker-compose up` from the root of the project. Then open a browser on http://localhost:8080.

## Deployment

To deploy to Google Kubernetes Engine (GKE), you need to follow these steps.

The values in the `.env` file should be set in the variables of you CI/CD tool.

### Authentication on GKE:
Have your GKE service account JSON data. To facilitate 1 liner in an environment variable, it is recommended that you base64 encode it using the following command:
```
cat service-account.json | base64 -w 0
```
then set the value `GKE_SERVICE_ACCOUNT` in the `.env` file.

You can now login with
```
./.ci/scripts/gke-login.sh
```

### First deployment

Create the cluster:
- Edit the `.env` file and set the values according to your GKE account (to create a zonal cluster, i.e. non-autopilot, change the GKE_AUTOPILOT to N)
- Then run the command
```
./.ci/scripts/gke-create-cluster.sh
```
- Deploy the load-balancer service
```
kubectl apply -f ./.ci/k8s/service.yaml
```

### Application deployment
To deploy a new version, update the container version in the `./.ci/k8s/service.yaml` then run
```
kubectl apply -f ./.ci/k8s/deployment.yaml
```
